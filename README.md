### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### Project Info

- This is meant to serve as a reference application for the Pipeline COE. 
- This application leverages the built-in GitLab Springboot Template to create a local SQLite instance onto a local Tomcat server via a Maven build. 
- Using the [chinook test data set](https://github.com/lerocha/chinook-database), CRUD functionality is built in
- Only the Employee test data is being used as of now - the artist test data was used initially for testing purposes
- The UI layer was built using and [Thymeleaf](https://www.thymeleaf.org/) as a Java server side engine and [bootstrap](https://getbootstrap.com/) for styling. 

### Prerequisites

To run this app as is, you need Java SE 8, over version 1.8, as specified in the pom.xml file.

This application also uses [Thymeleaf](https://www.thymeleaf.org/) and [bootstrap](https://getbootstrap.com/) for the front end, so that and NPM are required as well. 

*Optional* You can download SQLite Studio to have a view of the db you create using this app. 

### Troubleshooting Tips 

Use underscores instead of camelcase for field names when updating the data.sql file (spring initializes data with this file)

A local sqlite.db as specified in the application.properties file should be created on success as well as a message at http://localhost:8080/

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.