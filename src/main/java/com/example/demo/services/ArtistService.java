package com.example.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import com.example.demo.repository.ArtistRepository;
import com.example.demo.entity.Artist;
import java.util.List;

@Service
public class ArtistService {

    @Autowired
    private ArtistRepository artistRepository; 

    @Transactional
    public String createArtist(Artist artist){
        try {
            if (!artistRepository.existsByName(artist.getName())){
                artist.setId(null == artistRepository.findMaxId()? 0 : artistRepository.findMaxId() + 1);
                artistRepository.save(artist);
                return "Artist record created successfully.";
            }else {
                return "Artist already exists in the database.";
            }
        }catch (Exception e){
            throw e;
        }
    }

    public List<Artist> readArtists(){
        return artistRepository.findAll();
    }

    @Transactional
    public String updateArtist(Artist artist){
        if (artistRepository.existsByName(artist.getName())){
            try {
                List<Artist> artists = artistRepository.findByName(artist.getName());
                artists.stream().forEach(a -> {
                    Artist artistToBeUpdated = artistRepository.findById(a.getId()).get();
                    artistToBeUpdated.setName(artist.getName());
                    artistToBeUpdated.setId(artist.getId());
                    artistRepository.save(artistToBeUpdated);
                });
                return "Artist record updated.";
            }catch (Exception e){
                throw e;
            }
        }else {
            return "Artist does not exist in the database.";
        }
    }

    @Transactional
    public String deleteArtist(Artist artist){
        if (artistRepository.existsByName(artist.getName())){
            try {
                List<Artist> artists = artistRepository.findByName(artist.getName());
                artists.stream().forEach(a -> {
                    artistRepository.delete(a);
                });
                return "Artist record deleted successfully.";
            }catch (Exception e){
                throw e;
            }

        }else {
            return "Artist does not exist";
        }
    }
    
    
}
