package com.example.demo.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.entity.Employee;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository; 

    /* JPA built in find all repository reads everything in our Sqlite Employee table */

    public List<Employee> readEmployees(){
        return employeeRepository.findAll();
    }

     /* Using my own function to set the employee ID of an added record - was having issues with spring's built in generated ID functionality */
     
    public void saveEmployee(Employee employee) {
        employee.setEmployeeId(null == employeeRepository.findMaxId()? 0 : employeeRepository.findMaxId() + 1);
        this.employeeRepository.save(employee); 
    }

    /* Because I'm generating an ID, when new entry is added, I just give it an ID of 1 plus the max. In this case, during an update, the record is simply saved while the previous version is deleted */
    public void updateSaveEmployee(Employee employee) {
        this.employeeRepository.save(employee); 
    }

    public Employee getEmployeeById(Integer id) {
        Optional<Employee> optional = employeeRepository.findById(id);
        Employee employee = null; 
        if (optional.isPresent()) {
            employee = optional.get();
        } else {
            throw new RuntimeException("Employee not found for id :: " + id);
        }

        return employee; 
    }

    public void deleteEmployeeById(Integer id) {
        this.employeeRepository.deleteById(id);
    }
    
}
