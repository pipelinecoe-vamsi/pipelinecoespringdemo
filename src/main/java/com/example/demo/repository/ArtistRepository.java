package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.entity.Artist;

import java.util.List;

@Repository
public interface ArtistRepository extends JpaRepository<Artist, Integer> {

    public boolean existsByName(String name); 
    public List<Artist> findByName(String name); 

    @Query("select max(a.id) from Artist a")
    public Integer findMaxId(); 


}
