package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

 @Entity
public class Artist {

    @Id
    private Integer artist_id;
    private String name;
 
    public Integer getId() {
        return artist_id;
    }
 
    public void setId(Integer artist_id) {
        this.artist_id = artist_id;
    }
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    
}
