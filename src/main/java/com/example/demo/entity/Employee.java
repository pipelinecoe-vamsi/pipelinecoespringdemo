package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

 @Entity
public class Employee {

    @Id
    private Integer employee_id;

    private Integer reports_to; 
    private String last_name;
    private String first_name;
    private String title;
    private String address;
    private String city;
    private String state;
    private String country;
    private String postal_code;
    private String phone;
    private String fax;
    private String email;
    private String birth_date;

    public Integer getEmployeeId() {
        return employee_id;
    }
 
    public void setEmployeeId(Integer employee_id) {
        this.employee_id = employee_id;
    }

    public Integer getReportsTo() {
        return reports_to;
    }
 
    public void setReportsTo(Integer reports_to) {
        this.reports_to = reports_to;
    }
 
 
    public String getLastName() {
        return last_name;
    }
 
    public void setLastName(String last_name) {
        this.last_name = last_name;
    }

    public String getFirstName() {
        return first_name;
    }
 
    public void setFirstName(String first_name) {
        this.first_name = first_name;
    }

    public String getTitle() {
        return title;
    }
 
    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }
 
    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }
 
    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }
 
    public void setState(String state) {
        this.state = state;
    }

    public String getPhone() {
        return phone;
    }
 
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }
 
    public void setFax(String fax) {
        this.fax = fax;
    }
 
    public String getEmail() {
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }
 
    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalCode() {
        return postal_code;
    }
 
    public void setPostalCode(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getBirthDate() {
        return birth_date;
    }
 
    public void setBirthDate(String birth_date) {
        this.birth_date = birth_date;
    }
 
    
}
