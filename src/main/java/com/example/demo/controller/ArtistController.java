// package com.example.demo.controller;

// import com.example.demo.entity.Artist;
// import com.example.demo.services.ArtistService;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.web.bind.annotation.RequestBody;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.web.bind.annotation.RestController;

// import java.util.List;

// @RestController
// public class ArtistController {

//     @Autowired
//     private ArtistService artistService;

//     // @RequestMapping(value = "", method = RequestMethod.GET)
//     // public String landing(){
//     //     return "SqlLite DB Created! Look at the data populated in the sqlite.db at the root of the pipelinecoespringdemo repo.";
//     // }

//     @RequestMapping(value = "info", method = RequestMethod.GET)
//     public String info(){
//         return "The application is up.";
//     }

//     @RequestMapping(value = "createartist", method = RequestMethod.POST)
//     public String createArtist(@RequestBody Artist artist){
//         return artistService.createArtist(artist);
//     }

//     @RequestMapping(value = "readartists", method = RequestMethod.GET)
//     public List<Artist> readArtists(){
//         return artistService.readArtists();
//     }

//     @RequestMapping(value = "updateartist", method = RequestMethod.PUT)
//     public String updateArtist(@RequestBody Artist artist){
//         return artistService.updateArtist(artist);
//     }

//     @RequestMapping(value = "deleteartist", method = RequestMethod.DELETE)
//     public String deleteArtist(@RequestBody Artist artist){
//         return artistService.deleteArtist(artist);
//     }
// }
