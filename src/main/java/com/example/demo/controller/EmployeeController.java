package com.example.demo.controller;

import com.example.demo.entity.Employee;
import com.example.demo.services.EmployeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class EmployeeController {

    /* This controller is used to map the different url headers for CRUD operations */

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/")
    public String getHomePage(Model model){
        model.addAttribute("employeeList", employeeService.readEmployees()); 
        return "homepage"; 
    }

    @GetMapping("/addEmployee")
    public String addEmployee(Model model){
        Employee employee = new Employee();
        model.addAttribute("employee", employee); 
        return "newemployee"; 
    }

    @PostMapping("/saveEmployee")
    public String saveEmployee(@ModelAttribute("employee") Employee employee) {
        employeeService.saveEmployee(employee); 
        return "redirect:/"; 
    }

    /* There's probably opportunity to refactor here - right now on an update, I'm doing a delete and then saving the new record */
    @PostMapping("/updateSaveEmployee/{id}")
    public String updateSaveEmployee(@ModelAttribute("employee") Employee employee, @PathVariable (value="id") Integer id) {
        this.employeeService.deleteEmployeeById(id);
        employeeService.updateSaveEmployee(employee); 
        return "redirect:/"; 
    }

    @GetMapping("/updateEmployee/{id}")
    public String updateEmployee(@PathVariable (value="id") Integer id, Model model){
        Employee employee = employeeService.getEmployeeById(id);
        model.addAttribute("employee", employee); 
        return "updateemployee"; 
    }

    @GetMapping("/deleteEmployee/{id}")
    public String deleteEmployee(@PathVariable (value="id") Integer id, Model model){
        this.employeeService.deleteEmployeeById(id);
        return "redirect:/"; 
    }
}
